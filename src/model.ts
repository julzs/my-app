export type Todo = {
  id?: number;
  text: string;
  completed: boolean;
};

export interface Action<P> {
  type: string;
  payload: P;
  error?: boolean;
  meta?: Object;
}

export type IState = Todo[];
