import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { combineReducers, Store, createStore } from 'redux';
import { Provider } from 'react-redux';
import todos from './index';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import 'todomvc-app-css/index.css';

const rootReducer = combineReducers({
  todos
});

const initialState = {};

declare var window: any;

const store: Store<any> = createStore(
  rootReducer,
  initialState,
  window.devToolsExtension ? window.devToolsExtension() : undefined
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();